#!/bin/bash

DIR=`pwd`

cp -R ./etc /
cp ./firewall.service /etc/systemd/system/firewall.service

cd /etc/firewall
echo '#ssh' >> ./public_tcp_ports
sudo ss -lpn4 | awk '/sshd/{n=split($5,arr,":"); print arr[n]}' >> ./public_tcp_ports
echo '# self' >> ./trusted_hosts
ip addr | awk '/inet / { sub("/.*", "", $2); print $2}' >> ./trusted_hosts # !!! delete lo???
echo '# admin' >> ./trusted_hosts
w -h -i | awk '{if (length($3)>8) print $3}' >> ./trusted_hosts

systemctl daemon-reload
systemctl enable firewall
systemctl start firewall

cd $DIR