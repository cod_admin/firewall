#!/bin/bash

# great firewall of china


# consts

CONFIG_DIR='/etc/firewall'

BEFORE_STARTED_DIR='/etc/fw1.d'
AFTER_STARTED_DIR='/etc/fw2.d'
BEFORE_STOPPED_DIR='/etc/fw3.d'
AFTER_STOPPED_DIR='/etc/fw4.d'

CHAIN_INPUT_NAME='firewall_input'
CHAIN_FORWARD_NAME='firewall_forward'
CHAIN_OUTPUT_NAME='firewall_output'

RM_COMMENTS='sed s/#.*//'

BLACKLIST=`$RM_COMMENTS "$CONFIG_DIR/blacklist"`
FORWARDED_HOSTS=`$RM_COMMENTS "$CONFIG_DIR/forwarded_hosts"`
PUBLIC_TCP_PORTS=`$RM_COMMENTS "$CONFIG_DIR/public_tcp_ports"`
PUBLIC_UDP_PORTS=`$RM_COMMENTS "$CONFIG_DIR/public_udp_ports"`
TRUSTED_HOSTS=`$RM_COMMENTS "$CONFIG_DIR/trusted_hosts"`


# functions

add_chain() {
    iptables -N $1 #2> /dev/null
    iptables -A $1 -j RETURN #2> /dev/null
    iptables -A $2 -j $1 #2> /dev/null
}


delete_chain() {
    iptables -D $2 -j $1 #2> /dev/null
    iptables -F $1 #2> /dev/null
    iptables -X $1 #2> /dev/null
}


hosts() {
    for host in ${@:3}; do
        iptables -I $1 -s $host -j $2 #2> /dev/null
    done
}


ports() {
    for port in ${@:4}; do
        iptables -I $1 -p $2 --dport $port -j $3 #2> /dev/null
    done
}


start() {
    run-parts "$BEFORE_STARTED_DIR"

    add_chain $CHAIN_INPUT_NAME INPUT
    add_chain $CHAIN_FORWARD_NAME FORWARD
    add_chain $CHAIN_OUTPUT_NAME OUTPUT

    hosts $CHAIN_INPUT_NAME ACCEPT $TRUSTED_HOSTS
    ports $CHAIN_INPUT_NAME tcp ACCEPT $PUBLIC_TCP_PORTS
    ports $CHAIN_INPUT_NAME udp ACCEPT $PUBLIC_UDP_PORTS
    hosts $CHAIN_INPUT_NAME DROP $BLACKLIST

    hosts $CHAIN_FORWARD_NAME ACCEPT $FORWARDED_HOSTS
    hosts $CHAIN_FORWARD_NAME DROP $BLACKLIST

    run-parts "$AFTER_STARTED_DIR"
}


status() {
    iptables -L -n
    to
    iptables -S
    to
    iptables -L -n -t nat
    to
    iptables -S -t nat
}


stop() {
    run-parts "$BEFORE_STOPPED_DIR"

    delete_chain $CHAIN_INPUT_NAME INPUT
    delete_chain $CHAIN_FORWARD_NAME FORWARD
    delete_chain $CHAIN_OUTPUT_NAME OUTPUT

    run-parts "$AFTER_STOPPED_DIR"
}


to() {
    case $1 in
        0 | 'in' )
            echo ${@:2} >&0
        ;;
        1 | 'out' )
            echo ${@:2}
        ;;
        2 | 'err' )
            echo ${@:2} >&2
        ;;
        * )
            echo $@
    esac
}


# begin

case $1 in
    start)
        start
        to 'firewall started...'
    ;;
    stop)
        stop
        to 'firewall stoped...'
    ;;
    restart)
        stop
        start
        to 'firewall restarted...'
    ;;
    status)
        status
    ;;
    *)
        to "usage: $0 {start|stop|restart|status}"
esac

# end
